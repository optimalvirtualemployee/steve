<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>
<!doctype html>
<html class="no-js" lang="eng">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Online Courses and Tutor</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
		<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url');?>/assets/img/favicon.ico">
		<!-- CSS here -->
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/bootstrap.min.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/owl.carousel.min.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/flaticon.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/slicknav.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/animate.min.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/magnific-popup.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/fontawesome-all.min.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/themify-icons.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/slick.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/nice-select.css">
            <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/style.css">
             <?php //wp_head(); ?>
   </head>

   <body>
       
    <!-- Preloader Start -->
 <!--    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="<?php bloginfo('template_url');?>/assets/img/logo.png" alt="">
                </div>
            </div>
        </div>
    </div> -->
    <!-- Preloader Start -->

    <header>
        <!-- Header Start -->
       <div class="header-area">
            <div class="main-header ">
                <div class="header-top top-bg d-none d-lg-block">
                   <div class="container-fluid">
                       <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left d-flex">
                                     <!-- Logo -->
                            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-3">
                                <div class="logo">
                                  <a href="index.html"><img src="<?php bloginfo('template_url');?>/assets/img/logo.png" alt=""></a>
                                </div>
                            </div>
                                   
                                </div>
                                <div class="header-info-right">

                                   <ul>                                          
                                       <li><a href="#" class="btn header-btn">Book a session >></a></li>
                                      
                                       <li class="ml-2"> <?php
									wp_nav_menu(
										array(
											'theme_location' => 'primary',
										)
									);
								?></li>
                                   </ul>
                                </div>
                            </div>
                       </div>
                   </div>
                </div>
               <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">                           
                            <div class="col-md-12 text-center">
                                <!-- Main-menu -->
                                <div class="main-menu d-none d-lg-block">
                                    <nav>                                                
                                        <ul id="navigation">                     
                                            <li><a href="#">Primary 2-6</a></li>
                                            <li><a href="#">Secondary 7-10</a></li>
                                            <li><a href="#"> Senior 11-12</a>
                                                <!-- <ul class="submenu">
                                                    <li><a href="#"> Step 1</a></li>
                                                    <li><a href="#"> Step 2</a></li>
                                                </ul> -->
                                            </li>
                                            <li class="float-right">
                                                <a class="call-us" 
                                                href="tel:+496170961709"> CALL US NOW</a> 
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div> 
                            <!-- <div class="col-xl-3 col-lg-3 col-md-5 col-sm-3 fix-card">
                                <ul class="header-right f-right d-none d-lg-block d-flex justify-content-between">
                                    <li class="d-none d-xl-block f-right ">
                                        <div class="form-box f-right ">
                                            <input type="text" name="Search" placeholder="Search products">
                                            <div class="search-icon">
                                                <i class="fas fa-search special-tag"></i>
                                            </div>
                                        </div>
                                     </li>
                                </ul>
                            </div> -->
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
       </div>
        <!-- Header End -->
    </header>

