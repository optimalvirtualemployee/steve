<?php 
/*
Template Name: Book seesion

*/
get_header();
?>
    <main>
        <!-- confirm purchase Start-->
        <section class="confirm-purchase-area">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-4 mt-5">
                            <h2>Payment Option</h2>
                        </div>
                    </div>
                </div>
                <div class="custom-payment-section p-4">
                    <h5>Choose Payment Method</h5>
                    <div class="payment-navtabs">
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <div class="form-item">
                                <label for="radio-1">
                                    <input type="radio" name="radios" id="radio-1"> <span>Credit / Debit Card</span>
                                </label>
                            </div>
                          </li>
                          <li class="nav-item">
                            <div class="form-item">
                                <label for="radio-2">
                                    <input type="radio" name="radios" id="radio-2"><span>Paypal</span>
                                </label>
                            </div>
                          </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="product-card">
                                <div class="product-card-header">
                                    <img src="<?php bloginfo('template_url');?>/assets/img/mastercard.png" alt="" class="img-fluid">
                                    <span>3 Credits</span>
                                </div>
                                <div class="product-card-body">
                                    <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h6>
                                </div>
                                <div class="product-card-footer text-right">
                                    <span>$30.00</span>
                                </div>
                            </div>
                            <div class="payment-cards">
                                <img src="<?php bloginfo('template_url');?>/assets/img/mastercard.png" alt="" class="img-fluid">
                                <img src="<?php bloginfo('template_url');?>/assets/img/visa.png" alt="" class="img-fluid">
                                <img src="<?php bloginfo('template_url');?>/assets/img/amex.png" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <div class="card p-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="card-name">Name on Card</label>
                                            <input type="text" class="form-control" id="card-name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="postal-code">Postal Code</label>
                                            <input type="text" class="form-control" id="postal-code">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="card-number">Card Number (no space or dashes)</label>
                                            <input type="number" class="form-control" id="card-number">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="expiry-date">Expiration (mm/yy)</label>
                                            <input type="text" class="form-control" id="expiry-date">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="security-code">CVV</label>
                                            <input type="text" class="form-control" id="security-code">
                                        </div>
                                    </div>
                                </div>
                                <div class="button-box">
                                    <a href="#" class="btn header-btn">Pay Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer(); ?>