<?php 
/*
Template Name: Teacher Calendar
*/
get_header();
?>

    <main>
        <!-- confirm purchase Start-->
        <section class="confirm-purchase-area">
            <div class="container">
              <div id="calendar"></div>
            </div>
        </section>
    </main>
<?php get_footer(); ?>