<?php 
/*
Template Name: Webinar
*/
get_header();
?>
    <main>

<section class="category-area py-5">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-4">
                            <div class="dt-sc-anytitle">
                                <h2>Webinar Session</h2>
                                <span></span>
                            </div>
                           
                        </div>
                    </div>
                </div>
               <?php echo do_shortcode('[webinars]'); ?>
            </div>
        </section>     

    </main>
<?php get_footer(); ?>