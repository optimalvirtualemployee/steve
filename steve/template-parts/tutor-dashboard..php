<?php 
/*
Template Name: Tutor dashboard.
*/
get_header();
?>
    <main>
        <!-- confirm purchase Start-->
        <section class="confirm-purchase-area">
            <div class="container">
                <div class="custom-dashboard-section p-4">
                  <?php the_field('desciption'); ?>
            </div>
        </section>
    </main>
<?php get_footer(); ?>