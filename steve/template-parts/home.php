<?php 
/*
Template Name: Home
*/
get_header();
?>


    <main>
        <!-- slider Area Start -->
        <div class="slider-area ">
            <!-- Mobile Menu -->
            <div class="slider-active">         
			<?php 
			$bgimage = get_field('background_img');
			$size = 'full'; // (thumbnail, medium, large, full or custom size)

			$image = get_field('banner_img');
			$size = 'full';
			?>      
                <div class="single-slider slider-height"<?php if( $bgimage ) { ?> style="background-image: url(<?php echo esc_url($bgimage['url']); ?>)" <?php } ?>>
                    <div class="container">
                        <div class="row d-flex align-items-center justify-content-between">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 d-none d-md-block">
                                <div class="hero__img" data-animation="bounceIn" data-delay=".4s">
                                	<?php if( $image ): ?>
                                    <img src="<?php echo esc_url($image['url']); ?>" alt="">
                                     <?php endif; ?>
                                </div>
                            </div>
                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-8">
		                    <div class="hero__caption">
		                    <span data-animation="fadeInRight" data-delay=".4s"><?php the_field('title');?> </span>
		                    <h1 data-animation="fadeInRight" data-delay=".6s"><?php the_field('heading');?></h1>
		                    <p data-animation="fadeInRight" data-delay=".8s"><?php the_field('short_description');?></p>
		                        <!-- Hero-btn -->
		                        <div class="hero__btn" data-animation="fadeInRight" data-delay="1s">
	                            <?php $link = get_field('button'); 
	                                  if($link):?>
			                    <a href="<?php echo $link['url']; ?>" class="btn hero-btn" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			                          <?php endif; ?>
		                        </div>
		                    </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <!-- slider Area End-->
        
        <!-- Category Area Start-->
        <section class="category-area py-5">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-4">
                            <div class="dt-sc-anytitle">
                                <h2><?php the_field('cat_heading'); ?></h2>
                                <span></span>
                            </div>
                            <p><?php the_field('cat_title'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                	<?php $classes = get_field('classes_category'); 
                	      if($classes):
                          foreach ($classes as $key => $value) {
                           $img = $value['image'];
                	?>
                    <div class="col-xl-4 col-lg-6">
                        <div class="single-category mb-30">
                            <a href="#">
                                <div class="category-img">
                                    <img src="<?php echo $img['url']; ?>" alt="">
                                </div>
                                <div class="category-caption">
                                     <?php if($value['title']):?><h2><?php echo $value['title'];?></h2><?php endif; ?>
                                     <?php if($value['short_description']):?><p><?php echo $value['short_description'];?></p><?php endif; ?>
                                </div>
                            </a>
                        </div>
                    </div>
                   <?php  } 
                        endif;
                    ?>
               
                </div>
            </div>
        </section>
        <!-- Category Area End-->
     
    
      
        <!-- Shop Method Start-->
        
        <!-- Brilliant Learning Method Start-->
  <!--       <div class="brilinat-learning pb-5 pt-5 d-none">
            <div class="container">
                <div class="section-tittle dt-sc-anytitle text-center">
                    <h2>Brill Learning</h2>
                    <span></span>
                </div>
                <div class="brill-learning-content pt-3 mt-5">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="card ">
                                <a class="img-card" href="#">
                                <img src="<?php bloginfo('template_url');?>/assets/img/learning-new.png" />
                              </a>
                                <div class="card-content">
                                    <h4 class="card-title">
                                        <a href="#"> Effective Learning</a>
                                    </h4>
                                    <p class="">Effective Learning from the comfort of your home.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="card ">
                                <a class="img-card" href="#">
                                <img src="<?php bloginfo('template_url');?>/assets/img/schedule.png" />
                              </a>
                                <div class="card-content">
                                    <h4 class="card-title">
                                        <a href="#"> Flexible to your schedule</a>
                                    </h4>
                                    <p class="">Book a session between 7am to 10pm. 7days a week.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="card ">
                                <a class="img-card" href="#">
                                <img src="<?php bloginfo('template_url');?>/assets/img/session.png" />
                              </a>
                                <div class="card-content">
                                    <h4 class="card-title">
                                        <a href="#">Revisit your session</a>
                                    </h4>
                                    <p class="">With your permission,the session is recorded and accessible to you.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="card ">
                                <a class="img-card" href="#">
                                <img src="<?php bloginfo('template_url');?>/assets/img/learning-goals.png" />
                              </a>
                                <div class="card-content">
                                    <h4 class="card-title">
                                        <a href="#"> Personalised to your needs</a>
                                    </h4>
                                    <p class="">Personalised to your needs and learning goals.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="card ">
                                <a class="img-card" href="#">
                                <img src="<?php bloginfo('template_url');?>/assets/img/contract.png" />
                              </a>
                                <div class="card-content">
                                    <h4 class="card-title">
                                        <a href="#"> No lock fee No sign up fees</a>
                                    </h4>
                                    <p class="">No lock in contracts and No sign up fees.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="card ">
                                <a class="img-card" href="#">
                                <img src="<?php bloginfo('template_url');?>/assets/img/practice.png" />
                              </a>
                                <div class="card-content">
                                    <h4 class="card-title">
                                        <a href="#"> Real-time Practice</a>
                                    </h4>
                                    <p class="">Real-time Practice Questions and Exam papers.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="card ">
                                <a class="img-card" href="#">
                                <img src="<?php bloginfo('template_url');?>/assets/img/progress.png" />
                              </a>
                                <div class="card-content">
                                    <h4 class="card-title">
                                        <a href="#"> Track your progress</a>
                                    </h4>
                                    <p class="">Track your progressand plan sessions with your tutor. Reach your goals</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="brilinat-learning-box p-5">
            <div class="container">
                <div class="section-tittle dt-sc-anytitle text-center">
                    <h2><?php the_field('brilinat_heading');?></h2>
                    <span></span>
                </div>
              <ul class="info-wrapper">

              	<?php $info = get_field('info_wrapper'); 
            	      if($info):
            	      $i = 0;
                      foreach ($info as $key => $value) {
                       $img = $value['image'];
                       $i++;
                	?>
                <li class="infobox-<?php echo $i; ?>">
                    <a class="row" href="#<?php echo $i; ?>">
                        <span class="icon">
                            <img src="<?php echo $img['url']; ?>" />
                        </span>
                        <span class="info-text">
                          <?php if($value['title']):?><h4><?php echo $value['title'];?></h4><?php endif; ?>
                          <?php if($value['short_description']):?><p><?php echo $value['short_description'];?></p><?php endif; ?>
                        </span>
                    </a>
                </li>
                <?php 
                     }
                 endif;
                ?>

              </ul>
            </div>
        </div>
        <div class="latest-wrapper pt-5 pb-3" style="background: #e7f8bc;">
            <div class="latest-area latest-height d-flex align-items-center">
                <div class="container">
                    <div class="row d-flex">
                        <div class="col-xl-5 col-lg-5 col-md-6 ">
                            <div class="latest-caption">
                                <?php if(get_field('latest_heading')):?><h2><?php the_field('latest_heading');?></h2><?php endif; ?>                               
                            </div>
                        </div>
                         <div class="col-xl-7 col-lg-7 col-md-6 ">
                            <div class="latest-subscribe-box d-flex">
                                    <div class="media contact-info mr-5">
                                        <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                                        <div class="media-body">
                                            <?php if(get_field('phone')):?><h3><?php the_field('phone');?></h3><?php endif; ?>
                                            <?php if(the_field('days')):?><p><?php the_field('days');?></p><?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="media contact-info">
                                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                                        <div class="media-body">
                                            <form>
                                              <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                              </div>
                                              <div class="form-group">
                                                <label for="message">Message</label>
                                                <textarea class="form-control" placeholder="Password" 
                                                id="message" rows="3"></textarea>
                                              </div>
                                              <button type="submit" 
                                              class="btn header-btn">Submit</button>
                                            </form>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>               
            </div>
        </div>
        <div class="tutoring-method-area pt-5">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="col-xl-12 col-12 text-center mb-5">
                        <?php if(get_field('tutoring_heading')):?><h2 class=""><?php the_field('tutoring_heading');?></h2><?php endif;?>
                        <?php if(get_field('tutoring_title')):?><h5 class=""><?php the_field('tutoring_title');?></h5><?php endif;?>
                    </div>
                    <div class="col-xl-12 col-12 text-center mb-5">
                    <div class="single-element-widget mt-30" data-animation="fadeInRight" data-delay=".4s">
                        <h6 class="mb-30">School years</h6>
                        <div class="default-select" id="default-select">
                            <select>
                            <option value="3">Year 3</option>
                            <option value="4">Year 4</option>
                            <option value="5">Year 5</option>
                            <option value="6">Year 6</option>
                            <option value="7">Year 7</option>
                            </select>
                        </div>
                    </div>
                    <a href="#" class="btn header-btn mt-4">Next Step >></a>
                    </div>
                   
                </div>
            </div>
        </div>
        <div class="testimonial ">
            <div class="container">
                <div class="row">
                    <div class="col-xl">
                        <div class="testimonials-section">
                            <input type="radio" name="slider" title="slide1" checked="checked" class="slider__nav"/>
                            <input type="radio" name="slider" title="slide2" class="slider__nav"/>
                            <input type="radio" name="slider" title="slide3" class="slider__nav"/>
                            <input type="radio" name="slider" title="slide4" class="slider__nav"/>
                            <input type="radio" name="slider" title="slide5" class="slider__nav"/>
                          <div class="slider__inner">

							<?php $testimonial = get_field('testimonial'); 
							if($testimonial):
							foreach ($testimonial as $key => $value) {
							?>
                            <div class="slider__contents">
                              <quote>&rdquo;</quote>
                              <p class="slider__txt"><?php echo $value['slider_txt'];?></p>
                              <h2 class="slider__caption"><?php echo $value['slider_caption'];?></h2>
                            </div>
                        <?php } endif;?>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

<?php get_footer(); ?>