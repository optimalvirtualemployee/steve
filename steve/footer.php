<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
   <footer>

       <!-- Footer Start-->
       <div class="footer-area footer-padding pb-0">
           <div class="container">
               <div class="row d-flex justify-content-between">
                   <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6">
                      <div class="single-footer-caption mb-50">
                        <div class="single-footer-caption mb-30">
                             <!-- logo -->
                            <div class="footer-logo">
                                <a href="index.html"><img src="assets/img/footer-logo.png" alt=""></a>
                            </div>
                            <div class="footer-tittle">
                                <div class="footer-pera">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore.</p>
                               </div>
                            </div>
                        </div>
                      </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-5">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Quick Links</h4>
                               <ul>
                                <li><a href="#">Primary 2-6</a></li>
                                <li><a href="#">Secondary 7-10</a></li>
                                <li><a href="#">Senior 11-12</a></li>
                                   <li><a href="#">About</a></li>
                                   <li><a href="#"> Offers & Discounts</a></li>
                                   <li><a href="#"> Get Coupon</a></li>
                                   <li><a href="#">  Contact Us</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-7">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Community</h4>
                               <ul>
                                   <li><a href="#">Learners</a></li>
                                   <li><a href="#">Partners</a></li>
                                   <li><a href="#">Teaching Center</a></li>
                                   <li><a href="#">Blog</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-5 col-sm-7">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>More</h4>
                               <ul>
                                <li><a href="#">Frequently Asked Questions</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Report a Payment Issue</a></li>
                            </ul>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- Footer bottom -->
               <div class="row">
                <div class="col-xl-7 col-lg-7 col-md-7">
                    <div class="footer-copy-right">
                        <p>
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <a href="#">Steve</a>
  </p>                   </div>
                </div>
                 <div class="col-xl-5 col-lg-5 col-md-5">
                    <div class="footer-copy-right f-right">
                        <!-- social -->
                        <div class="footer-social" style="font-size: 0.5rem;">
                            <a href="#"><i class="fab fa-twitter fa-2x"></i></a>
                            <a href="#"><i class="fab fa-facebook-f fa-2x"></i></a>
                            <a href="#"><i class="fab fa-behance fa-2x"></i></a>
                            <a href="#"><i class="fas fa-globe fa-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
           </div>
       </div>
       <!-- Footer End-->

   </footer>
   
	<!-- JS here -->
	
		<!-- All JS Custom Plugins Link Here here -->
        <script href="<?php bloginfo('template_url');?>/assets/js/vendor/modernizr-3.5.0.min.js"></script>
		<!-- Jquery, Popper, Bootstrap -->
		<script href="<?php bloginfo('template_url');?>/assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/vendor/moment.min.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/vendor/fullcalendar.min.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/popper.min.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/bootstrap.min.js"></script>
	    <!-- Jquery Mobile Menu -->
        <script href="<?php bloginfo('template_url');?>/assets/js/jquery.slicknav.min.js"></script>

		<!-- Jquery Slick , Owl-Carousel Plugins -->
        <script href="<?php bloginfo('template_url');?>/assets/js/owl.carousel.min.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/slick.min.js"></script>

		<!-- One Page, Animated-HeadLin -->
        <script href="<?php bloginfo('template_url');?>/assets/js/wow.min.js"></script>
		<script href="<?php bloginfo('template_url');?>/assets/js/animated.headline.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/jquery.magnific-popup.js"></script>

		<!-- Scrollup, nice-select, sticky -->
        <script href="<?php bloginfo('template_url');?>/assets/js/jquery.scrollUp.min.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/jquery.nice-select.min.js"></script>
		<script href="<?php bloginfo('template_url');?>/assets/js/jquery.sticky.js"></script>
        
        <!-- contact js -->
        <script href="<?php bloginfo('template_url');?>/assets/js/contact.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/jquery.form.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/jquery.validate.min.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/mail-script.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/jquery.ajaxchimp.min.js"></script>
        
		<!-- Jquery Plugins, main Jquery -->	
        <script href="<?php bloginfo('template_url');?>/assets/js/plugins.js"></script>
        <script href="<?php bloginfo('template_url');?>/assets/js/main.js"></script>
        <?php wp_footer(); ?>
    </body>
</html>

