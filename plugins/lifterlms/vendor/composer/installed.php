<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-trunk',
    'version' => 'dev-trunk',
    'aliases' => 
    array (
    ),
    'reference' => '88985bffea54dc206ea483b3b3fcf1f816682389',
    'name' => 'gocodebox/lifterlms',
  ),
  'versions' => 
  array (
    'gocodebox/lifterlms' => 
    array (
      'pretty_version' => 'dev-trunk',
      'version' => 'dev-trunk',
      'aliases' => 
      array (
      ),
      'reference' => '88985bffea54dc206ea483b3b3fcf1f816682389',
    ),
    'lifterlms/lifterlms-blocks' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '59f2bf6ffb103f534f817ddb861d3fd80cba65de',
    ),
    'lifterlms/lifterlms-rest' => 
    array (
      'pretty_version' => '1.0.0-beta.16',
      'version' => '1.0.0.0-beta16',
      'aliases' => 
      array (
      ),
      'reference' => '714d17a6c997487214e8d3e94b64fbc8e8a1c253',
    ),
    'woocommerce/action-scheduler' => 
    array (
      'pretty_version' => '3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '275d0ba54b1c263dfc62688de2fa9a25a373edf8',
    ),
  ),
);
