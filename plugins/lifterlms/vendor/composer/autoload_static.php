<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8453a576565ffe779960829d9760ec2f
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'LLMS\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'LLMS\\' => 
        array (
            0 => __DIR__ . '/../..' . '/includes',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8453a576565ffe779960829d9760ec2f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8453a576565ffe779960829d9760ec2f::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit8453a576565ffe779960829d9760ec2f::$classMap;

        }, null, ClassLoader::class);
    }
}
